#### What is it?
Itsbullet is a conky weather theme based on [Harmattan](https://github.com/zagortenay333/Harmattan) and that uses the OpenWeatherMap service.  
It's main goal is to run smoother and provides new features.

#### Main differences with Harmattan
* Harmattan can monitor system resource and weather. Itsbullet is only a weather 
conky theme.
* Harmattan offers about 22 different themes. Itsbullet has only one unique theme 
based on Zukitwo.
* Harmattan has 4 Form factors. No Mini or God-Mode here, Itsbullet provides only 
one.

#### New features
* Config and scripts files have been rewritten.
* Weather data are downloaded every 20 minutes and parsed every 7 minutes.
* Added cloudiness and forecast temperature for the current day.
* Support time format (12 or 24 hours).
* Added  8 icons set and 2 photos set, that can be choosen in the conky.conf.
* Offline mode support.
* Added an errors log file.

#### Installation
1. Required dependencies: [conky](https://github.com/brndnmtthws/conky), 
[curl](https://curl.haxx.se), [jq](https://stedolan.github.io/jq) and 
["Droid Sans" font](https://www.fontsquirrel.com/fonts/droid-sans).

2. Move **conky.conf** file in ~/.config/conky.  
If you are using FreeType =< 2.7.1, you should use the appropriate version of the file.

3. Move **itsbullet** folder in ~/.local/share.  
Note that this location can be modified in the conky.conf.

4. Move both scripts (**get-weather** and **parse-weather**) in a folder that is set in 
The PATH environment variable.  
If you won't or can't do that just copy the scripts in /usr/local/bin.

5. Now, you must edit conky.conf and fill the templates, **all** fields are mandatory.  
Then, run conky and voila.

#### Troubleshooting
If the conky does not work correctly or nothing is displayed, you can refer back
to the errors log file, located  
by default in ~/.itsbullet_error.log.

Note that the errors log file can be disabled, by creating a **no_error_log** file 
in $XDG_CONFIG_HOME/conky.

![](screenshots/screen1.png "itsbullet with icon and photo style")
