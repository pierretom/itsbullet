#!/bin/bash

CACHE_DIR="$1"
DATA_SRC="$2"
DATA_PARSE="$3"
DAY_FCAST="$4"
CURRENT_WEATHER_FILE="$CACHE_DIR/current.json"
FORECAST_FILE="$CACHE_DIR/forecast.json"
ERR_LOG_FILE=~/.itsbullet_error
NO_ERR_LOG_FILE="$XDG_CONFIG_HOME/conky/no_error_log"
JQ="$(which jq)"

usage()
{
	if [ ! -f "$NO_ERR_LOG_FILE" ]
	then
		echo "[$(date +%F\ %T)] Usage: parse-weather <cache_dir> <data_source> <data_parse> [day_forecast]" >> "$ERR_LOG_FILE"
	fi

	exit 1
}

if [ "$#" -eq 0 -o "$#" -lt 3 -o "$#" -gt 4 ]
then
	usage
elif [ "$2" -eq 2 -a "$#" -lt 4 ]
then
	usage
fi

set_default_values()
{
	if [[ "$DATA_PARSE" = ".weather[0].icon" ]]
	then
		echo "null"
	else
		echo ""
	fi

	return 0
}

is_file_exist()
{
	local f="$1"

	if [ -s "$f" ]
	then
		return 0
	else
		local x=0
		local timeout=10

		while [ "$x" -lt $timeout -a ! -s "$f" ]; do
			x=$((x+1))
			sleep 1
		done

		if [ -s "$f" ]
		then
			return 0
		else
			return 1
		fi
	fi
}

test_data_src()
{
	case $DATA_SRC in
	1)
		is_file_exist "$CURRENT_WEATHER_FILE"
		if [ "$?" -ne 0 ]
		then
			if [ ! -f "$NO_ERR_LOG_FILE" ]
			then
				echo "[$(date +%F\ %T)] Data source not ready: $CURRENT_WEATHER_FILE." >> "$ERR_LOG_FILE"
			fi
			return 1
		else
			VERIFIED_DATA_SRC="$CURRENT_WEATHER_FILE"
		fi
		;;
	2)
		is_file_exist "$FORECAST_FILE"
		if [ "$?" -ne 0 ]
		then
			if [ ! -f "$NO_ERR_LOG_FILE" ]
			then
				echo "[$(date +%F\ %T)] Data source not ready: $FORECAST_FILE." >> "$ERR_LOG_FILE"
			fi
			return 1
		else
			VERIFIED_DATA_SRC="$FORECAST_FILE"
		fi
		;;
	*)
		if [ ! -f "$NO_ERR_LOG_FILE" ]
		then
			echo "[$(date +%F\ %T)] Allowed values for data source are:" >> "$ERR_LOG_FILE"
			echo "[$(date +%F\ %T)] 1 (current weather)" >> "$ERR_LOG_FILE"
			echo "[$(date +%F\ %T)] 2 (forecast)" >> "$ERR_LOG_FILE"
		fi
		set_default_values
		exit 1
		;;
	esac

	return 0
}

test_json_update()
{
	local json_mtime json_dt_timeout

	if [ -s "$CURRENT_WEATHER_FILE" ]
	then
		json_mtime=$(ls -o --time-style=+%s ${CURRENT_WEATHER_FILE} | cut -d ' ' -f 5)
		json_dt_timeout=$(($json_mtime+1800))

		if [ "$(date +%s)" -gt "$json_dt_timeout" ]
		then
			if [ ! -f "$NO_ERR_LOG_FILE" ]
			then
				echo "[$(date +%F\ %T)] Outdated data for $CURRENT_WEATHER_FILE." >> "$ERR_LOG_FILE"
			fi
			return 1
		fi
	fi

	if [ -s "$FORECAST_FILE" ]
	then
		json_mtime=$(ls -o --time-style=+%s ${FORECAST_FILE} | cut -d ' ' -f 5)
		json_dt_timeout=$(($json_mtime+1800))

		if [ "$(date +%s)" -gt "$json_dt_timeout" ]
		then
			if [ ! -f "$NO_ERR_LOG_FILE" ]
			then
				echo "[$(date +%F\ %T)] Outdated data for $FORECAST_FILE." >> "$ERR_LOG_FILE"
			fi
			return 1
		fi
	fi

	return 0
}

test_day_forecast()
{
	case $DAY_FCAST in
	"+0day"|"+1day"|"+2day"|"+3day")
		VERIFIED_DAY_FCAST="$DAY_FCAST"
		return 0
		;;
	*)
		if [ ! -f "$NO_ERR_LOG_FILE" ]
		then
			echo "[$(date +%F\ %T)] Allowed values for day forecast are:" >> "$ERR_LOG_FILE"
			echo "[$(date +%F\ %T)] +0day" >> "$ERR_LOG_FILE"
			echo "[$(date +%F\ %T)] +1day" >> "$ERR_LOG_FILE"
			echo "[$(date +%F\ %T)] +2day" >> "$ERR_LOG_FILE"
			echo "[$(date +%F\ %T)] +3day" >> "$ERR_LOG_FILE"
		fi
		set_default_values
		exit 1
		;;
	esac

	return 1
}

test_service_response()
{
	local code

	code=$($JQ -r ".cod" "$VERIFIED_DATA_SRC")
	if [ -z "$code" -o "$code" = "200" ]
	then
		return 0
	fi

	if [ "$DATA_SRC" -eq 1 ]
	then
		if [ ! -f "$NO_ERR_LOG_FILE" ]
		then
			local msg

			msg=$($JQ -r ".message" "$VERIFIED_DATA_SRC")
			echo "[$(date +%F\ %T)] Error returned from OpenWeatherMap service \
for current weather: $code - $msg." >> "$ERR_LOG_FILE"
		fi
		return 1
	fi

	if [ "$DATA_SRC" -eq 2 ]
	then
		if [ ! -f "$NO_ERR_LOG_FILE" ]
		then
			local msg

			msg=$($JQ -r ".message" "$VERIFIED_DATA_SRC")
			echo "[$(date +%F\ %T)] Error returned from OpenWeatherMap service \
for weather forecast: $code - $msg." >> "$ERR_LOG_FILE"
		fi
		return 1
	fi

	return 1
}

locate_day_forecast()
{
	local i timestamp conv_ts
	local cnt=$($JQ -r ".cnt" "$VERIFIED_DATA_SRC")
	local conv_day_fcast=$(date -d "$VERIFIED_DAY_FCAST" +%d)

	for ((i=0; i<$cnt; i++))
	do
		timestamp=$($JQ -r ".list[$i].dt" "$VERIFIED_DATA_SRC")
		conv_ts=$(date -d @"$timestamp" +%d)

		if [ "$conv_ts" = "$conv_day_fcast" ]
		then
			FORECAST_LIST=".list[$i]"
			return 0
		fi
	done

	return 1
}

get_data()
{
	local v
	local reg='[a-zA-Z]'

	if [ "$DATA_SRC" -eq 1 ]
	then
		v=$($JQ -r "$DATA_PARSE" "$VERIFIED_DATA_SRC")
		
		if [ -z "$v" -o "$v" = "null" ]
		then
			if [ ! -f "$NO_ERR_LOG_FILE" ]
			then
				echo "[$(date +%F\ %T)] Data to parse for current weather not found: $DATA_PARSE." >> "$ERR_LOG_FILE"
			fi
			set_default_values
			exit 1
		fi

		if [[ $v =~ $reg ]]
		then
			echo "$v"
		else
			if [[ $v =~ '-' ]]
			then
				echo "$v" | awk '{print int($1-0.5)}'
			else
				echo "$v" | awk '{print int($1+0.5)}'
			fi
		fi

		return 0
	fi

	if [ "$DATA_SRC" -eq 2 ]
	then
		locate_day_forecast
		if [ "$?" -eq 1 ]
		then
			if [ ! -f "$NO_ERR_LOG_FILE" ]
			then
				echo "[$(date +%F\ %T)] Can't locate day forecast: $VERIFIED_DAY_FCAST." >> "$ERR_LOG_FILE"
			fi
			set_default_values
			exit 1
		else
			local full_data_parse="$FORECAST_LIST$DATA_PARSE"

			v=$($JQ -r "$full_data_parse" "$VERIFIED_DATA_SRC")

			if [ -z "$v" -o "$v" = "null" ]
			then
				if [ ! -f "$NO_ERR_LOG_FILE" ]
				then
					echo "[$(date +%F\ %T)] Data to parse for weather forecast not found: $full_data_parse." >> "$ERR_LOG_FILE"
				fi
				set_default_values
				exit 1
			fi

			if [[ $v =~ $reg ]]
			then
				echo "$v"
			else
				if [[ $v =~ '-' ]]
				then
					echo "$v" | awk '{print int($1-0.5)}'
				else
					echo "$v" | awk '{print int($1+0.5)}'
				fi
			fi

			return 0
		fi
	fi

	return 1
}

main()
{
	test_data_src
	if [ "$?" -eq 1 ]
	then
		set_default_values
		exit 1
	fi

	test_json_update
	if [ "$?" -eq 1 ]
	then
		set_default_values
		exit 1
	fi

	if [ "$DATA_SRC" -eq 2 ]
	then
		test_day_forecast
		if [ "$?" -eq 1 ]
		then
			if [ ! -f "$NO_ERR_LOG_FILE" ]
			then
				echo "[$(date +%F\ %T)] Invalid day forecast." >> "$ERR_LOG_FILE"
			fi
			set_default_values
			exit 1
		fi
	fi

	if [ ! -x "$JQ" ]
	then
		if [ ! -f "$NO_ERR_LOG_FILE" ]
		then
			echo "[$(date +%F\ %T)] jq program not found, aborting." >> "$ERR_LOG_FILE"
		fi
		set_default_values
		exit 1
	fi

	test_service_response
	if [ "$?" -eq 1 ]
	then
		set_default_values
		exit 1
	else
		get_data
	fi
}

main
